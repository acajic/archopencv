#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>


int main(int argc, char** argv) {
	 
	cv::Mat img = cv::imread("../ArchOpenCV/image.jpg");
	std::cout << "Image size " << img.cols << "x" << img.rows << std::endl;

	//cv::imshow("Image", img);
	//cv::waitKey();

	return 0;
}